// The IMU Body display frame.
//
// (c) 2016 - 2018: Marcel Admiraal

#ifndef IMUBODYFRAME_H
#define IMUBODYFRAME_H

#include <wx/sizer.h>

#include "imubodyview.h"
#include "imubodycontrol.h"
#include "imubodypanel.h"

#include "controlframe.h"
#include "oscilloscopepanel.h"

class IMUBodyFrame : public ma::ControlFrame, IMUBodyView
{
public:
    /**
     * Default constructor.
     */
    IMUBodyFrame();

    /**
     * Destructor.
     */
    virtual ~IMUBodyFrame();

    virtual void updateGyroscopeData(
            const ma::Vector data, const unsigned int imu);

    virtual void updateMMGData(
            const ma::Vector data, const unsigned int imu);

    virtual void updateJointData(
            const double data, const unsigned int joint);

private:
    void configureIMUData(wxCommandEvent& event);
    void configureIMUData();
    void placeIMUs(wxCommandEvent& event);
    void placeIMUs();
    void setViewDirection(wxCommandEvent& event);
    void createTools();
    void clearDataPanels();
    void createDataPanels();

    IMUBodyControl* imuBodyControl;
    IMUBodyPanel* imuBodyPanel;
    wxBoxSizer* dataPanelSizer;

    ma::OscilloscopePanel** gyroscopeDataPanel;
    ma::OscilloscopePanel** jointAnglePanel;
    ma::OscilloscopePanel** mmgDataPanel;

    unsigned int imus;
};

#endif // IMUBODYFRAME_H
