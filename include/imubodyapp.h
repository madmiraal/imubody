// An application for displaying body movements captured using IMUs.
//
// (c) 2016: Marcel Admiraal

#ifndef IMUBODYAPP_H
#define IMUBODYAPP_H

#include <wx/app.h>

class IMUBodyApp : public wxApp
{
public:
    /**
     * Initialises the application.
     *
     * @return True to continue processing.
     */
    virtual bool OnInit();
};

#endif // IMUBODYAPP_H
