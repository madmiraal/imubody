// The IMU Body View interface.
//
// (c) 2018: Marcel Admiraal

#ifndef IMUBODYVIEW_H
#define IMUBODYVIEW_H

#include "vector.h"

class IMUBodyView
{
public:
    virtual void updateGyroscopeData(
            const ma::Vector data, const unsigned int imu) = 0;
    virtual void updateMMGData(
            const ma::Vector data, const unsigned int imu) = 0;
    virtual void updateJointData(
            const double data, const unsigned int joint) = 0;
};

#endif // IMUBODYVIEW_H
