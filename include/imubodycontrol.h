// The IMU Body Controller.
//
// (c) 2016 - 2018: Marcel Admiraal

#ifndef IMUBODYCONTROL_H
#define IMUBODYCONTROL_H

#include "control.h"
#include "imubodyview.h"
#include "imuplacement.h"

#include "quaternion.h"
#include "imudatacollector.h"

class IMUBodyControl : public ma::Control
{
public:
    /**
     * Constructor.
     */
    IMUBodyControl(IMUBodyView* view);

    /**
     * Default destructor
     */
    virtual ~IMUBodyControl();

    /**
     * Returns a pointer to the underlying data collector.
     *
     * @return A pointer to the underlying data collector.
     */
    ma::IMUDataCollector* getDataCollector();

    /**
     * Returns a pointer to the underlying IMU placements.
     *
     * @return A pointer to the underlying IMU placements.
     */
    IMUPlacement* getIMUPlacement();

    /**
     * Returns the array of location names.
     *
     * @return The array of location names.
     */
    const wxString* getLocationName() const;

    /**
     * Returns the number of locations.
     *
     * @return The number of locations.
     */
    unsigned int getLocations() const;

private:
    void loopFunction();
    double getJointAngle(const ma::Quaternion& quaternion1,
        const ma::Quaternion& quaternion2);

    // View.
    IMUBodyView* view;
    // IMU data collection.
    ma::IMUDataCollector dataCollector;
    // IMU Placements.
    IMUPlacement imuPlacement;

    // Location names.
    static const unsigned int locations;
    static const wxString locationName[];
};

#endif // IMUBODYCONTROL_H
