// Class for storing the location and orientation of IMUs.
//
// (c) 2018: Marcel Admiraal

#ifndef IMUPLACEMENT_H
#define IMUPLACEMENT_H

#include "quaternion.h"

#include <iostream>

class IMUPlacement
{
public:
    /**
     * Default constructor.
     */
    IMUPlacement();

    /**
     * Destructor.
     */
    virtual ~IMUPlacement();

    /**
     * Returns the IMU's starting orientation.
     *
     * @param imu   The IMU's starting orientation required.
     * @return      The IMU's starting orientation.
     */
    ma::Quaternion getOrientation(const unsigned int imu) const;

    /**
     * Sets the IMU's starting orientation.
     *
     * @param orientation   The IMU's starting orientation.
     * @param imu           The IMU's starting orientation to set.
     */
    void setOrientation(const ma::Quaternion orientation,
            const unsigned int imu);

    /**
     * Returns the IMU's location index.
     *
     * @param imu   The IMU's location index requried.
     * @return      The IMU's location index.
     */
    unsigned int getLocationIndex(const unsigned int index) const;

    /**
     * Sets the IMU's location index.
     *
     * @param orientation   The IMU's location index.
     * @param imu           The IMU's location index to set.
     */
    void setLocationIndex(unsigned int locationIndex,
            const unsigned int imu);

    /**
     * Returns the number of imus.
     *
     * @return The number of imus.
     */
    unsigned int getIMUs() const;

    /**
     * Sets the number of imus.
     *
     * @param imus  The number of imus.
     */
    void setIMUs(unsigned int imus);

    /**
     * Saves the IMU placements in the file specified.
     *
     * @param filename  The filename of the file.
     * @return          Whether or not the save was successful.
     */
    bool saveIMUPlacements(const char* filename);

    /**
     * Loads the IMU placements from the file specified.
     *
     * @param filename  The filename of the file.
     * @return          Whether or not the load was successful.
     */
    bool loadIMUPlacements(const char* filename);

private:
    unsigned int imus;
    unsigned int* locationIndex;
    ma::Quaternion* orientation;
};

/**
 * Output stream operator.
 * Appends the IMU placements to the output stream and
 * returns the output stream.
 *
 * @param os     The output stream.
 * @param source The IMU placements to append to the output stream.
 * @return       The output stream.
 */
std::ostream& operator<<(std::ostream& os, const IMUPlacement& source);

/**
 * Input stream operator.
 * Inputs the IMU placements into the destination (if possible) and
 * returns the input stream.
 *
 * @param is          The input stream.
 * @param destination The IMU placements to input into.
 * @return            The input stream.
 */
std::istream& operator>>(std::istream& is, IMUPlacement& destination);

#endif // IMUPLACEMENT_H
