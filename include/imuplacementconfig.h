// Dialog for specifying the location and orientation of IMUs.
//
// (c) 2018: Marcel Admiraal

#ifndef IMUPLACEMENTCONFIG_H
#define IMUPLACEMENTCONFIG_H

#include <wx/dialog.h>
#include <wx/sizer.h>
#include <wx/combobox.h>
#include <wx/spinctrl.h>
#include <wx/string.h>

#include "imuplacement.h"

#include "vector3d.h"
#include "quaternion.h"

#include "cuboidframe.h"

class IMUPlacementConfig : public wxDialog
{
public:
    /**
     * Constructor.
     *
     * @param parent        The window that owns this dialog.
     * @param imuPlacement  Pointer to IMU placements.
     * @param locationNames Array of location name strings.
     * @param locations     The number of locations.
     */
    IMUPlacementConfig(wxWindow* parent, IMUPlacement* imuPlacement,
        const wxString* locationNames, const unsigned int locations);

    /**
     * Destructor.
     */
    virtual ~IMUPlacementConfig();

private:
    void setNumIMUs(wxCommandEvent& event);
    void locationChanged(wxCommandEvent& event);
    void xDirectionChanged(wxCommandEvent& event);
    void yDirectionChanged(wxCommandEvent& event);
    void zDirectionChanged(wxCommandEvent& event);
    void loadIMUPlacements(wxCommandEvent& event);
    void saveIMUPlacements(wxCommandEvent& event);
    void doneButton(wxCommandEvent& event);

    void clearComboArrays();
    void populatePlacementSizer();

    wxString getXDirection(ma::Quaternion orientation);
    wxString getYDirection(ma::Quaternion orientation);
    wxString getZDirection(ma::Quaternion orientation);
    wxString getDirection(ma::Vector3D);
    void setXDirection(const unsigned int imu);
    void setYDirection(const unsigned int imu);
    void setZDirection(const unsigned int imu);
    void setOrientation(const unsigned int imu);
    void setCuboidOrientation(const ma::Quaternion orientation,
        const unsigned int imu);

    IMUPlacement* imuPlacement;
    wxSpinCtrl* numIMUsSpinner;
    wxBoxSizer* imuPlacementSizer;
    wxComboBox** xDirectionCombo;
    wxComboBox** yDirectionCombo;
    wxComboBox** zDirectionCombo;
    const wxString* locationNames;
    const unsigned int locations;

    static const int directions;
    static const wxString directionName[];

    ma::CuboidFrame* cuboidFrame;
};

#endif // IMUPLACEMENTCONFIG_H
