// A wxPanel to display the body.
//
// (c) 2014 - 2018: Marcel Admiraal

#ifndef IMUBODYPANEL_H
#define IMUBODYPANEL_H

#include <wx/panel.h>

#include "imubodycontrol.h"
#include "quaternion.h"

class IMUBodyPanel : public wxPanel
{
public:
    /**
     * Constructor.
     *
     * @param parent            Pointer to the parent window.
     * @param imuBodyControl    Pointer to the controller.
     */
    IMUBodyPanel(wxWindow* parent, IMUBodyControl* imuBodyControl);

    /**
     * Destructor.
     */
    virtual ~IMUBodyPanel();

    /**
     * Set the view orientation.
     *
     * @param orienation    The new view orientation.
     */
    void setViewOrientation(const ma::Quaternion& orientation);

private:
    // Updates the vector points used to draw the figure.
    void updateBodyPoints(const ma::Vector3D bodyLine[]);
    // Stop erase event function.
    void stopErase(wxEraseEvent& event);
    // Paint event function.
    void paint(wxPaintEvent& event);
    // Idle event function
    void onIdle(wxIdleEvent& evt);

    IMUBodyControl* imuBodyControl;
    ma::Quaternion viewOrientation;
    static const unsigned int bodyPoints;
    ma::Vector3D* bodyPoint;
    static const unsigned int bodyLines;
    static const ma::Vector3D initialBodyLine[];
    ma::Vector3D* bodyLine;
//    ma::Vector3D bodyPoint[BODY_POINTS];
//    double angle;
};

#endif // IMUBODYPANEL_H
