// (c) 2016 - 2018: Marcel Admiraal

#include "imubodyframe.h"

#include <wx/stattext.h>

#include "imuconfig.h"
#include "imuplacement.h"
#include "imuplacementconfig.h"

#include "icons.h"

#define OSCILLOSCOPE_PERIOD 1000

enum ViewDirection
{
    Left,
    Right,
    Top,
    Bottom,
    Front,
    Back,
    Isometric
};

IMUBodyFrame::IMUBodyFrame() :
    ma::ControlFrame(new IMUBodyControl(this), 0,
        wxID_ANY, "IMU Body", wxDefaultPosition, wxSize(481, 466)),
    gyroscopeDataPanel(0), jointAnglePanel(0), mmgDataPanel(0), imus(0)
{
    imuBodyControl = (IMUBodyControl*)control;

    SetIcon(wxIcon(wxT("stickman.ico")));

    // Create configure IMUs menu items.
    wxMenu* inputMenu = new wxMenu();
    inputMenu->Append(wxID_EXECUTE, wxT("Configure IMU Data"),
            wxT("Configure the IMU Data Sources."));
    Bind(wxEVT_COMMAND_MENU_SELECTED, &IMUBodyFrame::configureIMUData, this,
            wxID_EXECUTE);
    inputMenu->Append(wxID_CONVERT, wxT("Place IMUs"),
            wxT("Specify the placement of the IMUs on the body."));
    Bind(wxEVT_COMMAND_MENU_SELECTED, &IMUBodyFrame::placeIMUs, this,
            wxID_CONVERT);
    menuBar->Append(inputMenu, wxT("Inputs"));

    // Create toolbar tools.
    createTools();

    // Create panels.
    wxBoxSizer* frameSizer = new wxBoxSizer(wxHORIZONTAL);
    SetSizer(frameSizer);
    // Create body panel.
    imuBodyPanel = new IMUBodyPanel(this, imuBodyControl);
    frameSizer->Add(imuBodyPanel, 2, wxEXPAND);
    // Create data panels.
    dataPanelSizer = new wxBoxSizer(wxVERTICAL);
    frameSizer->Add(dataPanelSizer, 1, wxEXPAND);
    createDataPanels();
    imuBodyControl->pauseLoop(false);
}

IMUBodyFrame::~IMUBodyFrame()
{
    imuBodyControl->pauseLoop(true);
    clearDataPanels();
}

void IMUBodyFrame::updateGyroscopeData(
        const ma::Vector data, const unsigned int imu)
{
    if (imu < imus)
        gyroscopeDataPanel[imu]->addData(data);
}

void IMUBodyFrame::updateMMGData(
        const ma::Vector data, const unsigned int imu)
{
    if (imu < imus)
        mmgDataPanel[imu]->addData(data);
}

void IMUBodyFrame::updateJointData(
        const double data, const unsigned int joint)
{
    if (joint < imus / 2)
        jointAnglePanel[joint]->addData(data);
}

void IMUBodyFrame::configureIMUData(wxCommandEvent& event)
{
    configureIMUData();
}

void IMUBodyFrame::configureIMUData()
{
    bool paused = imuBodyControl->isLoopPaused();
    imuBodyControl->pauseLoop(true);
    ma::IMUConfig dialog(this, imuBodyControl->getDataCollector());
    dialog.ShowModal();
    if (imuBodyControl->getDataCollector()->getIMUs() !=
        imuBodyControl->getIMUPlacement()->getIMUs())
    {
        imuBodyControl->getIMUPlacement()->setIMUs(
                imuBodyControl->getDataCollector()->getIMUs());
        placeIMUs();
    }
    if (imuBodyControl->getDataCollector()->getIMUs() != imus)
    {
        clearDataPanels();
        createDataPanels();
    }
    imuBodyControl->pauseLoop(paused);
}

void IMUBodyFrame::placeIMUs(wxCommandEvent& event)
{
    placeIMUs();
}

void IMUBodyFrame::placeIMUs()
{
    bool paused = imuBodyControl->isLoopPaused();
    imuBodyControl->pauseLoop(true);
    {
        IMUPlacementConfig dialog(this,
                imuBodyControl->getIMUPlacement(),
                imuBodyControl->getLocationName(),
                imuBodyControl->getLocations());
        dialog.ShowModal();
    }
    if (imuBodyControl->getDataCollector()->getIMUs() !=
        imuBodyControl->getIMUPlacement()->getIMUs())
    {
        imuBodyControl->getDataCollector()->setIMUs(
                imuBodyControl->getIMUPlacement()->getIMUs());
        configureIMUData();
    }
    imuBodyControl->pauseLoop(paused);
}

void IMUBodyFrame::setViewDirection(wxCommandEvent& event)
{
    ma::Quaternion orientation(ma::Vector3D(1,0,0), 0);
    switch (event.GetId())
    {
    case ViewDirection::Front:
        orientation = ma::Quaternion(ma::Vector3D(1, 0, 0), 0);
        break;
    case ViewDirection::Back:
        orientation = ma::Quaternion(ma::Vector3D(0, 0, 1), M_PI);
        break;
    case ViewDirection::Left:
        orientation = ma::Quaternion(ma::Vector3D(0, 0, 1), M_PI/2);
        break;
    case ViewDirection::Right:
        orientation = ma::Quaternion(ma::Vector3D(0, 0, 1), -M_PI/2);
        break;
    case ViewDirection::Top:
        orientation = ma::Quaternion(ma::Vector3D(1, 0, 0), M_PI/2);
        break;
    case ViewDirection::Bottom:
        orientation = ma::Quaternion(ma::Vector3D(1, 0, 0), -M_PI/2);
        break;
    case ViewDirection::Isometric:
        orientation = ma::Quaternion(ma::Vector3D(0, 0, 1), -M_PI/4);
        orientation *= ma::Quaternion(ma::Vector3D(1, 1, 0), M_PI/4);
        break;
    }
    imuBodyPanel->setViewOrientation(orientation);
}

void IMUBodyFrame::createTools()
{
    toolBar->AddTool(ViewDirection::Front, wxT("Front View"),
            front3DCube, wxNullBitmap, wxITEM_RADIO,
            wxT("View from the front"), wxT("View from the front."));
    Bind(wxEVT_COMMAND_TOOL_CLICKED, &IMUBodyFrame::setViewDirection, this,
            ViewDirection::Front);

    toolBar->AddTool(ViewDirection::Back, wxT("Back View"),
            back3DCube, wxNullBitmap, wxITEM_RADIO,
            wxT("View from the back"), wxT("View from the back."));
    Bind(wxEVT_COMMAND_TOOL_CLICKED, &IMUBodyFrame::setViewDirection, this,
            ViewDirection::Back);

    toolBar->AddTool(ViewDirection::Left, wxT("Left View"),
            left3DCube, wxNullBitmap, wxITEM_RADIO,
            wxT("View from the left"), wxT("View from the left."));
    Bind(wxEVT_COMMAND_TOOL_CLICKED, &IMUBodyFrame::setViewDirection, this,
            ViewDirection::Left);

    toolBar->AddTool(ViewDirection::Right, wxT("Right View"),
            right3DCube, wxNullBitmap, wxITEM_RADIO,
            wxT("View from the right"), wxT("View from the right."));
    Bind(wxEVT_COMMAND_TOOL_CLICKED, &IMUBodyFrame::setViewDirection, this,
            ViewDirection::Right);

    toolBar->AddTool(ViewDirection::Top, wxT("Top View"),
            top3DCube, wxNullBitmap, wxITEM_RADIO,
            wxT("View from the top"), wxT("View from the top."));
    Bind(wxEVT_COMMAND_TOOL_CLICKED, &IMUBodyFrame::setViewDirection, this,
            ViewDirection::Top);

    toolBar->AddTool(ViewDirection::Bottom, wxT("Bottom View"),
            bottom3DCube, wxNullBitmap, wxITEM_RADIO,
            wxT("View from the bottom"), wxT("View from the bottom."));
    Bind(wxEVT_COMMAND_TOOL_CLICKED, &IMUBodyFrame::setViewDirection, this,
            ViewDirection::Bottom);

    toolBar->AddTool(ViewDirection::Isometric, wxT("Isometric View"),
            none3DCube, wxNullBitmap, wxITEM_RADIO,
            wxT("View from isometric view"), wxT("View from isometric view."));
    Bind(wxEVT_COMMAND_TOOL_CLICKED, &IMUBodyFrame::setViewDirection, this,
            ViewDirection::Isometric);

    toolBar->Realize();
}

void IMUBodyFrame::clearDataPanels()
{
    imus = 0;
    dataPanelSizer->Clear(true);
    if (gyroscopeDataPanel != 0) delete[] gyroscopeDataPanel;
    if (jointAnglePanel != 0) delete[] jointAnglePanel;
    if (mmgDataPanel != 0) delete[] mmgDataPanel;
}

void IMUBodyFrame::createDataPanels()
{
    unsigned int newIMUs = imuBodyControl->getDataCollector()->getIMUs();
    gyroscopeDataPanel = new ma::OscilloscopePanel*[newIMUs]();
    jointAnglePanel = new ma::OscilloscopePanel*[newIMUs/2]();
    mmgDataPanel = new ma::OscilloscopePanel*[newIMUs]();

    // Create gyroscope data panels.
    dataPanelSizer->Add(new wxStaticText(this, wxID_ANY, wxT("Gyroscope Data")));
    for (unsigned int imu = 0; imu < newIMUs; ++imu)
    {
        gyroscopeDataPanel[imu] = new ma::OscilloscopePanel(
                this, 3, OSCILLOSCOPE_PERIOD, -1000, 1000);
        dataPanelSizer->Add(gyroscopeDataPanel[imu], 1, wxEXPAND);
    }
    // Create joint angle panels.
    dataPanelSizer->Add(new wxStaticText(this, wxID_ANY, wxT("Joint Angles")));
    for (unsigned int joint = 0; joint < newIMUs/2; ++joint)
    {
        jointAnglePanel[joint] = new ma::OscilloscopePanel(
                this, 1, OSCILLOSCOPE_PERIOD, -3.2, 3.2);
        dataPanelSizer->Add(jointAnglePanel[joint], 1, wxEXPAND);
    }
    // Create MMG data panels.
    dataPanelSizer->Add(new wxStaticText(this, wxID_ANY, wxT("MMG Data")));
    for (unsigned int imu = 0; imu < newIMUs; ++imu)
    {
        mmgDataPanel[imu] = new ma::OscilloscopePanel(
                this, 8, OSCILLOSCOPE_PERIOD, 0, 1);
        dataPanelSizer->Add(mmgDataPanel[imu], 1, wxEXPAND);
    }
    dataPanelSizer->Layout();
    imus = newIMUs;
}
