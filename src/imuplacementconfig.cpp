// (c) 2018: Marcel Admiraal

#include "imuplacementconfig.h"

#include <wx/stattext.h>
#include <wx/button.h>
#include <wx/filedlg.h>
#include <wx/msgdlg.h>

#include <cmath>

enum Direction
{
    Right,
    Left,
    Up,
    Down,
    Forward,
    Back
};

IMUPlacementConfig::IMUPlacementConfig(wxWindow* parent,
        IMUPlacement* imuPlacement, const wxString* locationNames,
        const unsigned int locations) :
    wxDialog(parent, wxID_ANY, wxT("Place IMUs")), imuPlacement(imuPlacement),
    locationNames(locationNames), locations(locations)
{
    wxBoxSizer* configIMUPlacementSizer = new wxBoxSizer(wxVERTICAL);
    // Number of IMUs
    wxBoxSizer* numIMUsSizer = new wxBoxSizer(wxHORIZONTAL);
    numIMUsSizer->Add(
            new wxStaticText(this, wxID_ANY, wxT("Number of IMUs: ")),
            0, wxALIGN_LEFT | wxALL, 5);
    numIMUsSpinner = new wxSpinCtrl(this, wxID_ANY, wxEmptyString,
            wxDefaultPosition, wxDefaultSize,
            wxSP_ARROW_KEYS | wxALIGN_RIGHT, 1, 8, imuPlacement->getIMUs());
    numIMUsSpinner->Bind(wxEVT_SPINCTRL, &IMUPlacementConfig::setNumIMUs, this);
    numIMUsSizer->Add(numIMUsSpinner);
    configIMUPlacementSizer->Add(numIMUsSizer);

    imuPlacementSizer = new wxBoxSizer(wxVERTICAL);
    populatePlacementSizer();
    configIMUPlacementSizer->Add(imuPlacementSizer);

    // Load and Save Buttons.
    wxBoxSizer* buttonSizer = new wxBoxSizer(wxHORIZONTAL);
    wxButton* loadButton = new wxButton(this, wxID_ANY, wxT("Load"));
    wxButton* saveButton = new wxButton(this, wxID_ANY, wxT("Save"));
    wxButton* doneButton = new wxButton(this, wxID_ANY, wxT("Done"));
    loadButton->Bind(wxEVT_BUTTON, &IMUPlacementConfig::loadIMUPlacements, this);
    saveButton->Bind(wxEVT_BUTTON, &IMUPlacementConfig::saveIMUPlacements, this);
    doneButton->Bind(wxEVT_BUTTON, &IMUPlacementConfig::doneButton, this);
    buttonSizer->AddStretchSpacer(1);
    buttonSizer->Add(loadButton, 0, wxALL, 5);
    buttonSizer->Add(saveButton, 0, wxALL, 5);
    buttonSizer->Add(doneButton, 0, wxALL, 5);
    buttonSizer->AddStretchSpacer(1);
    configIMUPlacementSizer->Add(buttonSizer, 0, wxEXPAND);

    SetSizerAndFit(configIMUPlacementSizer);
    SetSizeHints(-1, -1);

    cuboidFrame = new ma::CuboidFrame(this);
    cuboidFrame->Show();
    setCuboidOrientation(imuPlacement->getOrientation(0), 0);
//    cuboidFrame->SetTitle(wxT("IMU0 Orientation"));
//    ma::Quaternion orientation = imuPlacement->getOrientation(0);
//    orientation = ma::Quaternion(ma::Vector3D(0,0,1), 0.1) * orientation;
//    cuboidFrame->setOrientation(orientation);
}

IMUPlacementConfig::~IMUPlacementConfig()
{
    clearComboArrays();
}

void IMUPlacementConfig::setNumIMUs(wxCommandEvent& event)
{
    unsigned int imus = event.GetSelection();
    imuPlacement->setIMUs(imus);
    clearComboArrays();
    populatePlacementSizer();
}

void IMUPlacementConfig::locationChanged(wxCommandEvent& event)
{
    unsigned int imu = event.GetId();
    imuPlacement->setLocationIndex(event.GetSelection(), imu);
    setOrientation(imu);
}

void IMUPlacementConfig::xDirectionChanged(wxCommandEvent& event)
{
    unsigned int imu = event.GetId() - imuPlacement->getIMUs();
    unsigned int xDirection = xDirectionCombo[imu]->GetCurrentSelection();
    unsigned int yDirection = yDirectionCombo[imu]->GetCurrentSelection();
    if (xDirection / 2 == yDirection / 2)
    {
        setYDirection(imu);
    }
    else
    {
        setZDirection(imu);
    }
    setOrientation(imu);
}

void IMUPlacementConfig::yDirectionChanged(wxCommandEvent& event)
{
    unsigned int imu = event.GetId() - 2 * imuPlacement->getIMUs();
    unsigned int xDirection = xDirectionCombo[imu]->GetCurrentSelection();
    unsigned int yDirection = yDirectionCombo[imu]->GetCurrentSelection();
    if (xDirection / 2 == yDirection / 2)
    {
        setXDirection(imu);
    }
    else
    {
        setZDirection(imu);
    }
    setOrientation(imu);
}

void IMUPlacementConfig::zDirectionChanged(wxCommandEvent& event)
{
    unsigned int imu = event.GetId() - 3 * imuPlacement->getIMUs();
    unsigned int xDirection = xDirectionCombo[imu]->GetCurrentSelection();
    unsigned int zDirection = zDirectionCombo[imu]->GetCurrentSelection();
    if (xDirection / 2 == zDirection / 2)
    {
        setXDirection(imu);
    }
    else
    {
        setYDirection(imu);
    }
    setOrientation(imu);
}

void IMUPlacementConfig::loadIMUPlacements(wxCommandEvent& event)
{
    bool success = false;
    while (!success)
    {
        wxFileDialog openFileDialog(this, wxT("Load IMU Placements"),
                wxEmptyString, wxT("imuplacements.dat"),
                wxFileSelectorDefaultWildcardStr,
                wxFD_OPEN|wxFD_FILE_MUST_EXIST);
        if (openFileDialog.ShowModal() == wxID_CANCEL) return;
        success = imuPlacement->loadIMUPlacements(openFileDialog.GetPath());
        if (!success)
        {
            wxMessageBox(wxT("Load Failed"));
        }
    }
}

void IMUPlacementConfig::saveIMUPlacements(wxCommandEvent& event)
{
    bool success = false;
    while (!success)
    {
        wxFileDialog saveFileDialog(this, wxT("Save IMU Placements"),
                wxEmptyString, wxT("imuplacements.dat"),
                wxFileSelectorDefaultWildcardStr,
                wxFD_SAVE|wxFD_OVERWRITE_PROMPT);
        if (saveFileDialog.ShowModal() == wxID_CANCEL) return;
        success = imuPlacement->saveIMUPlacements(saveFileDialog.GetPath());
        if (!success)
        {
            wxMessageBox(wxT("Save Failed"));
        }
    }
}

void IMUPlacementConfig::doneButton(wxCommandEvent& event)
{
    Close();
}

void IMUPlacementConfig::clearComboArrays()
{
    delete[] xDirectionCombo;
    delete[] yDirectionCombo;
    delete[] zDirectionCombo;
}

void IMUPlacementConfig::populatePlacementSizer()
{
    unsigned int imus = imuPlacement->getIMUs();
    xDirectionCombo = new wxComboBox*[imus];
    yDirectionCombo = new wxComboBox*[imus];
    zDirectionCombo = new wxComboBox*[imus];

    imuPlacementSizer->Clear(true);
    for (unsigned int imu = 0; imu < imus; ++imu)
    {
        wxBoxSizer* rowSizer = new wxBoxSizer(wxHORIZONTAL);
        wxString imuName = wxT("IMU "); imuName << imu; imuName << ": ";

        rowSizer->Add(
                new wxStaticText(this, wxID_ANY, imuName), 0,
                wxALIGN_LEFT | wxALL, 5);
        wxComboBox* location = new wxComboBox(
                this, imu, locationNames[imuPlacement->getLocationIndex(imu)],
                wxDefaultPosition, wxDefaultSize, locations, locationNames,
                wxCB_READONLY);
        rowSizer->Add(location);
        location->Bind(wxEVT_COMBOBOX,
                &IMUPlacementConfig::locationChanged, this);

        rowSizer->Add(
                new wxStaticText(this, wxID_ANY, wxT("X:")), 0,
                wxALIGN_LEFT | wxALL, 5);
        xDirectionCombo[imu] = new wxComboBox(
                this, imu + imus,
                getXDirection(imuPlacement->getOrientation(imu)),
                wxDefaultPosition, wxDefaultSize, directions, directionName,
                wxCB_READONLY);
        rowSizer->Add(xDirectionCombo[imu]);
        xDirectionCombo[imu]->Bind(wxEVT_COMBOBOX,
                &IMUPlacementConfig::xDirectionChanged, this);

        rowSizer->Add(
                new wxStaticText(this, wxID_ANY, wxT("Y:")), 0,
                wxALIGN_LEFT | wxALL, 5);
        yDirectionCombo[imu] = new wxComboBox(
                this, imu + 2 * imus,
                getYDirection(imuPlacement->getOrientation(imu)),
                wxDefaultPosition, wxDefaultSize, directions, directionName,
                wxCB_READONLY);
        rowSizer->Add(yDirectionCombo[imu]);
        yDirectionCombo[imu]->Bind(wxEVT_COMBOBOX,
                &IMUPlacementConfig::yDirectionChanged, this);

        rowSizer->Add(
                new wxStaticText(this, wxID_ANY, wxT("Z:")), 0,
                wxALIGN_LEFT | wxALL, 5);
        zDirectionCombo[imu] = new wxComboBox(
                this, imu + 3 * imus,
                getZDirection(imuPlacement->getOrientation(imu)),
                wxDefaultPosition, wxDefaultSize, directions, directionName,
                wxCB_READONLY);
        rowSizer->Add(zDirectionCombo[imu]);
        zDirectionCombo[imu]->Bind(wxEVT_COMBOBOX,
                &IMUPlacementConfig::zDirectionChanged, this);

        imuPlacementSizer->Add(rowSizer);
    }
    Layout();
    Fit();
}

wxString IMUPlacementConfig::getXDirection(ma::Quaternion orientation)
{
    if (orientation == ma::Quaternion()) return wxEmptyString;
    ma::Vector3D xVector(1, 0, 0);
    xVector = ma::rotate(xVector, orientation);
    return getDirection(xVector);
}

wxString IMUPlacementConfig::getYDirection(ma::Quaternion orientation)
{
    if (orientation == ma::Quaternion()) return wxEmptyString;
    ma::Vector3D yVector(0, 1, 0);
    yVector = ma::rotate(yVector, orientation);
    return getDirection(yVector);
}

wxString IMUPlacementConfig::getZDirection(ma::Quaternion orientation)
{
    if (orientation == ma::Quaternion()) return wxEmptyString;
    ma::Vector3D zVector(0, 0, 1);
    zVector = ma::rotate(zVector, orientation);
    return getDirection(zVector);
}

wxString IMUPlacementConfig::getDirection(ma::Vector3D vector3D)
{
    double x = vector3D[0];
    double y = vector3D[1];
    double z = vector3D[2];
    double maximum = vector3D.max();
    double minimum = vector3D.min();
    // Positive directions
    if (maximum > -minimum)
    {
        if (x == maximum) return directionName[Direction::Right];
        if (y == maximum) return directionName[Direction::Forward];
        if (z == maximum) return directionName[Direction::Up];
    }
    // Negative directions
    else
    {
        if (x == minimum) return directionName[Direction::Left];
        if (y == minimum) return directionName[Direction::Back];
        if (z == minimum) return directionName[Direction::Down];
    }
    return (wxT("Error"));
}

void IMUPlacementConfig::setXDirection(const unsigned int imu)
{
    unsigned int yDirection = yDirectionCombo[imu]->GetSelection();
    unsigned int zDirection = zDirectionCombo[imu]->GetSelection();
    switch (yDirection)
    {
    case Direction::Right:
        switch (zDirection)
        {
        case Direction::Up:
            xDirectionCombo[imu]->SetSelection(Direction::Back);
            break;
        case Direction::Down:
            xDirectionCombo[imu]->SetSelection(Direction::Forward);
            break;
        case Direction::Forward:
            xDirectionCombo[imu]->SetSelection(Direction::Up);
            break;
        case Direction::Back:
            xDirectionCombo[imu]->SetSelection(Direction::Down);
            break;
        default:
            std::cerr << "Error: z direction cannot be ";
            std::cerr << directionName[zDirection];
            std::cerr << " when y direction is ";
            std::cerr << directionName[yDirection] << std::endl;
        }
        break;
    case Direction::Left:
        switch (zDirection)
        {
        case Direction::Up:
            xDirectionCombo[imu]->SetSelection(Direction::Forward);
            break;
        case Direction::Down:
            xDirectionCombo[imu]->SetSelection(Direction::Back);
            break;
        case Direction::Forward:
            xDirectionCombo[imu]->SetSelection(Direction::Down);
            break;
        case Direction::Back:
            xDirectionCombo[imu]->SetSelection(Direction::Up);
            break;
        default:
            std::cerr << "Error: z direction cannot be ";
            std::cerr << directionName[zDirection];
            std::cerr << " when y direction is ";
            std::cerr << directionName[yDirection] << std::endl;
        }
        break;
    case Direction::Up:
        switch (zDirection)
        {
        case Direction::Right:
            xDirectionCombo[imu]->SetSelection(Direction::Forward);
            break;
        case Direction::Left:
            xDirectionCombo[imu]->SetSelection(Direction::Back);
            break;
        case Direction::Forward:
            xDirectionCombo[imu]->SetSelection(Direction::Left);
            break;
        case Direction::Back:
            xDirectionCombo[imu]->SetSelection(Direction::Right);
            break;
        default:
            std::cerr << "Error: z direction cannot be ";
            std::cerr << directionName[zDirection];
            std::cerr << " when y direction is ";
            std::cerr << directionName[yDirection] << std::endl;
        }
        break;
    case Direction::Down:
        switch (zDirection)
        {
        case Direction::Right:
            xDirectionCombo[imu]->SetSelection(Direction::Back);
            break;
        case Direction::Left:
            xDirectionCombo[imu]->SetSelection(Direction::Forward);
            break;
        case Direction::Forward:
            xDirectionCombo[imu]->SetSelection(Direction::Right);
            break;
        case Direction::Back:
            xDirectionCombo[imu]->SetSelection(Direction::Left);
            break;
        default:
            std::cerr << "Error: z direction cannot be ";
            std::cerr << directionName[zDirection];
            std::cerr << " when y direction is ";
            std::cerr << directionName[yDirection] << std::endl;
        }
        break;
    case Direction::Forward:
        switch (zDirection)
        {
        case Direction::Right:
            xDirectionCombo[imu]->SetSelection(Direction::Down);
            break;
        case Direction::Left:
            xDirectionCombo[imu]->SetSelection(Direction::Up);
            break;
        case Direction::Up:
            xDirectionCombo[imu]->SetSelection(Direction::Right);
            break;
        case Direction::Down:
            xDirectionCombo[imu]->SetSelection(Direction::Left);
            break;
        default:
            std::cerr << "Error: z direction cannot be ";
            std::cerr << directionName[zDirection];
            std::cerr << " when y direction is ";
            std::cerr << directionName[yDirection] << std::endl;
        }
        break;
    case Direction::Back:
        switch (zDirection)
        {
        case Direction::Right:
            xDirectionCombo[imu]->SetSelection(Direction::Up);
            break;
        case Direction::Left:
            xDirectionCombo[imu]->SetSelection(Direction::Down);
            break;
        case Direction::Up:
            xDirectionCombo[imu]->SetSelection(Direction::Left);
            break;
        case Direction::Down:
            xDirectionCombo[imu]->SetSelection(Direction::Right);
            break;
        default:
            std::cerr << "Error: z direction cannot be ";
            std::cerr << directionName[zDirection];
            std::cerr << " when y direction is ";
            std::cerr << directionName[yDirection] << std::endl;
        }
        break;
    }
}

void IMUPlacementConfig::setYDirection(const unsigned int imu)
{
    unsigned int xDirection = xDirectionCombo[imu]->GetSelection();
    unsigned int zDirection = zDirectionCombo[imu]->GetSelection();
    switch (xDirection)
    {
    case Direction::Right:
        switch (zDirection)
        {
        case Direction::Up:
            yDirectionCombo[imu]->SetSelection(Direction::Forward);
            break;
        case Direction::Down:
            yDirectionCombo[imu]->SetSelection(Direction::Back);
            break;
        case Direction::Forward:
            yDirectionCombo[imu]->SetSelection(Direction::Down);
            break;
        case Direction::Back:
            yDirectionCombo[imu]->SetSelection(Direction::Up);
            break;
        default:
            std::cerr << "Error: z direction cannot be ";
            std::cerr << directionName[zDirection];
            std::cerr << " when x direction is ";
            std::cerr << directionName[xDirection] << std::endl;
        }
        break;
    case Direction::Left:
        switch (zDirection)
        {
        case Direction::Up:
            yDirectionCombo[imu]->SetSelection(Direction::Back);
            break;
        case Direction::Down:
            yDirectionCombo[imu]->SetSelection(Direction::Forward);
            break;
        case Direction::Forward:
            yDirectionCombo[imu]->SetSelection(Direction::Up);
            break;
        case Direction::Back:
            yDirectionCombo[imu]->SetSelection(Direction::Down);
            break;
        default:
            std::cerr << "Error: z direction cannot be ";
            std::cerr << directionName[zDirection];
            std::cerr << " when x direction is ";
            std::cerr << directionName[xDirection] << std::endl;
        }
        break;
    case Direction::Up:
        switch (zDirection)
        {
        case Direction::Right:
            yDirectionCombo[imu]->SetSelection(Direction::Back);
            break;
        case Direction::Left:
            yDirectionCombo[imu]->SetSelection(Direction::Forward);
            break;
        case Direction::Forward:
            yDirectionCombo[imu]->SetSelection(Direction::Right);
            break;
        case Direction::Back:
            yDirectionCombo[imu]->SetSelection(Direction::Left);
            break;
        default:
            std::cerr << "Error: z direction cannot be ";
            std::cerr << directionName[zDirection];
            std::cerr << " when x direction is ";
            std::cerr << directionName[xDirection] << std::endl;
        }
        break;
    case Direction::Down:
        switch (zDirection)
        {
        case Direction::Right:
            yDirectionCombo[imu]->SetSelection(Direction::Forward);
            break;
        case Direction::Left:
            yDirectionCombo[imu]->SetSelection(Direction::Back);
            break;
        case Direction::Forward:
            yDirectionCombo[imu]->SetSelection(Direction::Left);
            break;
        case Direction::Back:
            yDirectionCombo[imu]->SetSelection(Direction::Right);
            break;
        default:
            std::cerr << "Error: z direction cannot be ";
            std::cerr << directionName[zDirection];
            std::cerr << " when x direction is ";
            std::cerr << directionName[xDirection] << std::endl;
        }
        break;
    case Direction::Forward:
        switch (zDirection)
        {
        case Direction::Right:
            yDirectionCombo[imu]->SetSelection(Direction::Up);
            break;
        case Direction::Left:
            yDirectionCombo[imu]->SetSelection(Direction::Down);
            break;
        case Direction::Up:
            yDirectionCombo[imu]->SetSelection(Direction::Left);
            break;
        case Direction::Down:
            yDirectionCombo[imu]->SetSelection(Direction::Right);
            break;
        default:
            std::cerr << "Error: z direction cannot be ";
            std::cerr << directionName[zDirection];
            std::cerr << " when x direction is ";
            std::cerr << directionName[xDirection] << std::endl;
        }
        break;
    case Direction::Back:
        switch (zDirection)
        {
        case Direction::Right:
            yDirectionCombo[imu]->SetSelection(Direction::Down);
            break;
        case Direction::Left:
            yDirectionCombo[imu]->SetSelection(Direction::Up);
            break;
        case Direction::Up:
            yDirectionCombo[imu]->SetSelection(Direction::Right);
            break;
        case Direction::Down:
            yDirectionCombo[imu]->SetSelection(Direction::Left);
            break;
        default:
            std::cerr << "Error: z direction cannot be ";
            std::cerr << directionName[zDirection];
            std::cerr << " when x direction is ";
            std::cerr << directionName[xDirection] << std::endl;
        }
        break;
    }
}

void IMUPlacementConfig::setZDirection(const unsigned int imu)
{
    unsigned int xDirection = xDirectionCombo[imu]->GetSelection();
    unsigned int yDirection = yDirectionCombo[imu]->GetSelection();
    switch (xDirection)
    {
    case Direction::Right:
        switch (yDirection)
        {
        case Direction::Up:
            zDirectionCombo[imu]->SetSelection(Direction::Back);
            break;
        case Direction::Down:
            zDirectionCombo[imu]->SetSelection(Direction::Forward);
            break;
        case Direction::Forward:
            zDirectionCombo[imu]->SetSelection(Direction::Up);
            break;
        case Direction::Back:
            zDirectionCombo[imu]->SetSelection(Direction::Down);
            break;
        default:
            std::cerr << "Error: y direction cannot be ";
            std::cerr << directionName[yDirection];
            std::cerr << " when x direction is ";
            std::cerr << directionName[xDirection] << std::endl;
        }
        break;
    case Direction::Left:
        switch (yDirection)
        {
        case Direction::Up:
            zDirectionCombo[imu]->SetSelection(Direction::Forward);
            break;
        case Direction::Down:
            zDirectionCombo[imu]->SetSelection(Direction::Back);
            break;
        case Direction::Forward:
            zDirectionCombo[imu]->SetSelection(Direction::Down);
            break;
        case Direction::Back:
            zDirectionCombo[imu]->SetSelection(Direction::Up);
            break;
        default:
            std::cerr << "Error: y direction cannot be ";
            std::cerr << directionName[yDirection];
            std::cerr << " when x direction is ";
            std::cerr << directionName[xDirection] << std::endl;
        }
        break;
    case Direction::Up:
        switch (yDirection)
        {
        case Direction::Right:
            zDirectionCombo[imu]->SetSelection(Direction::Forward);
            break;
        case Direction::Left:
            zDirectionCombo[imu]->SetSelection(Direction::Back);
            break;
        case Direction::Forward:
            zDirectionCombo[imu]->SetSelection(Direction::Left);
            break;
        case Direction::Back:
            zDirectionCombo[imu]->SetSelection(Direction::Right);
            break;
        default:
            std::cerr << "Error: y direction cannot be ";
            std::cerr << directionName[yDirection];
            std::cerr << " when x direction is ";
            std::cerr << directionName[xDirection] << std::endl;
        }
        break;
    case Direction::Down:
        switch (yDirection)
        {
        case Direction::Right:
            zDirectionCombo[imu]->SetSelection(Direction::Back);
            break;
        case Direction::Left:
            zDirectionCombo[imu]->SetSelection(Direction::Forward);
            break;
        case Direction::Forward:
            zDirectionCombo[imu]->SetSelection(Direction::Right);
            break;
        case Direction::Back:
            zDirectionCombo[imu]->SetSelection(Direction::Left);
            break;
        default:
            std::cerr << "Error: y direction cannot be ";
            std::cerr << directionName[yDirection];
            std::cerr << " when x direction is ";
            std::cerr << directionName[xDirection] << std::endl;
        }
        break;
    case Direction::Forward:
        switch (yDirection)
        {
        case Direction::Right:
            zDirectionCombo[imu]->SetSelection(Direction::Down);
            break;
        case Direction::Left:
            zDirectionCombo[imu]->SetSelection(Direction::Up);
            break;
        case Direction::Up:
            zDirectionCombo[imu]->SetSelection(Direction::Right);
            break;
        case Direction::Down:
            zDirectionCombo[imu]->SetSelection(Direction::Left);
            break;
        default:
            std::cerr << "Error: y direction cannot be ";
            std::cerr << directionName[yDirection];
            std::cerr << " when x direction is ";
            std::cerr << directionName[xDirection] << std::endl;
        }
        break;
    case Direction::Back:
        switch (yDirection)
        {
        case Direction::Right:
            zDirectionCombo[imu]->SetSelection(Direction::Up);
            break;
        case Direction::Left:
            zDirectionCombo[imu]->SetSelection(Direction::Down);
            break;
        case Direction::Up:
            zDirectionCombo[imu]->SetSelection(Direction::Left);
            break;
        case Direction::Down:
            zDirectionCombo[imu]->SetSelection(Direction::Right);
            break;
        default:
            std::cerr << "Error: y direction cannot be ";
            std::cerr << directionName[yDirection];
            std::cerr << " when x direction is ";
            std::cerr << directionName[xDirection] << std::endl;
        }
        break;
    }
}

void IMUPlacementConfig::setOrientation(const unsigned int imu)
{
    ma::Quaternion orientation(ma::Vector3D(1, 0, 0), 0);
    unsigned int xDirection = xDirectionCombo[imu]->GetSelection();
    unsigned int yDirection = yDirectionCombo[imu]->GetSelection();
    switch (xDirection)
    {
    case Direction::Right:
        orientation = ma::Quaternion(ma::Vector3D(0, 0, 1), 0);
        switch (yDirection)
        {
        case Direction::Up:
            orientation *= ma::Quaternion(ma::Vector3D(1, 0, 0), M_PI / 2);
            break;
        case Direction::Down:
            orientation *= ma::Quaternion(ma::Vector3D(1, 0, 0), -M_PI / 2);
            break;
        case Direction::Forward:
            orientation *= ma::Quaternion(ma::Vector3D(1, 0, 0), 0);
            break;
        case Direction::Back:
            orientation *= ma::Quaternion(ma::Vector3D(1, 0, 0), M_PI);
            break;
        default:
            std::cerr << "Error: y direction cannot be ";
            std::cerr << directionName[yDirection];
            std::cerr << " when x direction is ";
            std::cerr << directionName[xDirection] << std::endl;
        }
        break;
    case Direction::Left:
        orientation = ma::Quaternion(ma::Vector3D(0, 0, 1), M_PI);
        switch (yDirection)
        {
        case Direction::Up:
            orientation *= ma::Quaternion(ma::Vector3D(1, 0, 0), M_PI / 2);
            break;
        case Direction::Down:
            orientation *= ma::Quaternion(ma::Vector3D(1, 0, 0), -M_PI / 2);
            break;
        case Direction::Forward:
            orientation *= ma::Quaternion(ma::Vector3D(1, 0, 0), M_PI);
            break;
        case Direction::Back:
            orientation *= ma::Quaternion(ma::Vector3D(1, 0, 0), 0);
            break;
        default:
            std::cerr << "Error: y direction cannot be ";
            std::cerr << directionName[yDirection];
            std::cerr << " when x direction is ";
            std::cerr << directionName[xDirection] << std::endl;
        }
        break;
    case Direction::Up:
        orientation = ma::Quaternion(ma::Vector3D(0, 1, 0), -M_PI / 2);
        switch (yDirection)
        {
        case Direction::Right:
            orientation *= ma::Quaternion(ma::Vector3D(1, 0, 0), -M_PI / 2);
            break;
        case Direction::Left:
            orientation *= ma::Quaternion(ma::Vector3D(1, 0, 0), M_PI / 2);
            break;
        case Direction::Forward:
            orientation *= ma::Quaternion(ma::Vector3D(1, 0, 0), 0);
            break;
        case Direction::Back:
            orientation *= ma::Quaternion(ma::Vector3D(1, 0, 0), M_PI);
            break;
        default:
            std::cerr << "Error: y direction cannot be ";
            std::cerr << directionName[yDirection];
            std::cerr << " when x direction is ";
            std::cerr << directionName[xDirection] << std::endl;
        }
        break;
    case Direction::Down:
        orientation = ma::Quaternion(ma::Vector3D(0, 1, 0), M_PI / 2);
        switch (yDirection)
        {
        case Direction::Right:
            orientation *= ma::Quaternion(ma::Vector3D(1, 0, 0), M_PI / 2);
            break;
        case Direction::Left:
            orientation *= ma::Quaternion(ma::Vector3D(1, 0, 0), -M_PI / 2);
            break;
        case Direction::Forward:
            orientation *= ma::Quaternion(ma::Vector3D(1, 0, 0), 0);
            break;
        case Direction::Back:
            orientation *= ma::Quaternion(ma::Vector3D(1, 0, 0), M_PI);
            break;
        default:
            std::cerr << "Error: y direction cannot be ";
            std::cerr << directionName[yDirection];
            std::cerr << " when x direction is ";
            std::cerr << directionName[xDirection] << std::endl;
        }
        break;
    case Direction::Forward:
        orientation = ma::Quaternion(ma::Vector3D(0, 0, 1), M_PI / 2);
        switch (yDirection)
        {
        case Direction::Right:
            orientation *= ma::Quaternion(ma::Vector3D(1, 0, 0), M_PI);
            break;
        case Direction::Left:
            orientation *= ma::Quaternion(ma::Vector3D(1, 0, 0), 0);
            break;
        case Direction::Up:
            orientation *= ma::Quaternion(ma::Vector3D(1, 0, 0), M_PI / 2);
            break;
        case Direction::Down:
            orientation *= ma::Quaternion(ma::Vector3D(1, 0, 0), -M_PI / 2);
            break;
        default:
            std::cerr << "Error: y direction cannot be ";
            std::cerr << directionName[yDirection];
            std::cerr << " when x direction is ";
            std::cerr << directionName[xDirection] << std::endl;
        }
        break;
    case Direction::Back:
        orientation = ma::Quaternion(ma::Vector3D(0, 0, 1), -M_PI / 2);
        switch (yDirection)
        {
        case Direction::Right:
            orientation *= ma::Quaternion(ma::Vector3D(1, 0, 0), 0);
            break;
        case Direction::Left:
            orientation *= ma::Quaternion(ma::Vector3D(1, 0, 0), M_PI);
            break;
        case Direction::Up:
            orientation *= ma::Quaternion(ma::Vector3D(1, 0, 0), M_PI / 2);
            break;
        case Direction::Down:
            orientation *= ma::Quaternion(ma::Vector3D(1, 0, 0), -M_PI / 2);
            break;
        default:
            std::cerr << "Error: y direction cannot be ";
            std::cerr << directionName[yDirection];
            std::cerr << " when x direction is ";
            std::cerr << directionName[xDirection] << std::endl;
        }
        break;
    }
    imuPlacement->setOrientation(orientation, imu);
    setCuboidOrientation(orientation, imu);
}

void IMUPlacementConfig::setCuboidOrientation(const ma::Quaternion orientation,
        const unsigned int imu)
{
    wxString title(wxT("IMU"));
    title << imu << wxT(" Orientation");
    cuboidFrame->SetTitle(title);
    cuboidFrame->setOrientation(ma::Quaternion(ma::Vector3D(1, 0, 1), .1) *
            orientation);
}

const int IMUPlacementConfig::directions = 6;

const wxString IMUPlacementConfig::directionName[] =
{
    wxT("Right"),
    wxT("Left"),
    wxT("Up"),
    wxT("Down"),
    wxT("Forward"),
    wxT("Back")
};
