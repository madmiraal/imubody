// (c) 2016: Marcel Admiraal

#include "imubodyapp.h"

#include "imubodyframe.h"

IMPLEMENT_APP(IMUBodyApp);

bool IMUBodyApp::OnInit()
{
    IMUBodyFrame* frame = new IMUBodyFrame();
    frame->Show();
    return true;
}
