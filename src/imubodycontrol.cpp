// (c) 2016 - 2018: Marcel Admiraal

#include "imubodycontrol.h"

#include "vector3d.h"

IMUBodyControl::IMUBodyControl(IMUBodyView* view) :
        ma::Control(), view(view)
{
}

IMUBodyControl::~IMUBodyControl()
{
}

ma::IMUDataCollector* IMUBodyControl::getDataCollector()
{
    return &dataCollector;
}

IMUPlacement* IMUBodyControl::getIMUPlacement()
{
    return &imuPlacement;
}

const wxString* IMUBodyControl::getLocationName() const
{
    return locationName;
}

unsigned int IMUBodyControl::getLocations() const
{
    return locations;
}

void IMUBodyControl::loopFunction()
{
    const unsigned int imus = dataCollector.getIMUs();
    for (unsigned int imu = 0; imu < imus; ++imu)
    {
        view->updateGyroscopeData(dataCollector.getCurrentGyroscope(imu), imu);
        view->updateMMGData(dataCollector.getCurrentAnalogueData(imu), imu);
    }
    for (unsigned int joint = 0; joint < imus/2; ++joint)
    {
        ma::Quaternion upper = dataCollector.getCurrentQuaternion(joint * 2);
        ma::Quaternion lower = dataCollector.getCurrentQuaternion(joint * 2 + 1);
        double angle = getJointAngle(upper, lower);
        if (joint % 2 > 0) angle *= -1;
        view->updateJointData(angle, joint);
    }
}

double IMUBodyControl::getJointAngle(const ma::Quaternion& quaternion1,
        const ma::Quaternion& quaternion2)
{
    ma::Vector3D vector1 = rotate(ma::Vector3D(1, 0, 0), quaternion1);
    ma::Vector3D vector2 = rotate(ma::Vector3D(1, 0, 0), quaternion2);
    double angle = acos(vector1.dot(vector2));
    if (vector1.cross(vector2)[2] < 0) angle *= -1;
    return angle;
}

const unsigned int IMUBodyControl::locations = 15;

const wxString IMUBodyControl::locationName[] =
{
    wxT(""),
    wxT("Head"),
    wxT("Body"),
    wxT("Shoulders"),
    wxT("Hips"),
    wxT("LeftUpperLeg"),
    wxT("RightUpperLeg"),
    wxT("LeftLowerLeg"),
    wxT("RightLowerLeg"),
    wxT("LeftFoot"),
    wxT("RightFoot"),
    wxT("LeftUpperArm"),
    wxT("RightUpperArm"),
    wxT("LeftLowerArm"),
    wxT("RightLowerArm")
};
