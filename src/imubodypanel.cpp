// (c) 2014 - 2018: Marcel Admiraal

#include "imubodypanel.h"

#include <wx/dcbuffer.h>

#include "vector3d.h"
#include "imudatacollector.h"

enum BodyPoint {
    Head,
    Nose,
    BodyTop,
    BodyBottom,
    LeftShoulder,
    RightShoulder,
    LeftHip,
    RightHip,
    LeftKnee,
    RightKnee,
    LeftHeel,
    RightHeel,
    LeftToe,
    RightToe,
    LeftElbow,
    RightElbow,
    LeftWrist,
    RightWrist
};

enum BodyLine {
    Forehead,
    Chin,
    Body,
    Shoulders,
    Hips,
    LeftUpperLeg,
    RightUpperLeg,
    LeftLowerLeg,
    RightLowerLeg,
    LeftFoot,
    RightFoot,
    LeftUpperArm,
    RightUpperArm,
    LeftLowerArm,
    RightLowerArm
};

IMUBodyPanel::IMUBodyPanel(wxWindow* parent, IMUBodyControl* imuBodyControl) :
        wxPanel(parent, wxID_ANY, wxDefaultPosition,
                wxDefaultSize, wxFULL_REPAINT_ON_RESIZE | wxSUNKEN_BORDER),
        imuBodyControl(imuBodyControl), viewOrientation(ma::Vector3D(1,0,0), 0),
        bodyPoint(new ma::Vector3D[bodyPoints]),
        bodyLine(new ma::Vector3D[bodyLines])
{
    SetBackgroundColour(*wxBLACK);
    Connect(wxEVT_ERASE_BACKGROUND, wxEraseEventHandler(IMUBodyPanel::stopErase));
    Connect(wxEVT_PAINT, wxPaintEventHandler(IMUBodyPanel::paint));
    Connect(wxEVT_IDLE, wxIdleEventHandler(IMUBodyPanel::onIdle));
}

IMUBodyPanel::~IMUBodyPanel()
{
    delete[] bodyPoint;
    delete[] bodyLine;
}

void IMUBodyPanel::setViewOrientation(const ma::Quaternion& orientation)
{
    viewOrientation = orientation;
}

void IMUBodyPanel::updateBodyPoints(const ma::Vector3D bodyLine[])
{
    bodyPoint[BodyPoint::LeftToe] = ma::Vector3D(-5, 5, -40);
    bodyPoint[BodyPoint::RightToe] = ma::Vector3D(5, 5, -40);
    bodyPoint[BodyPoint::LeftHeel] = bodyPoint[BodyPoint::LeftToe] -
            bodyLine[BodyLine::LeftFoot];
    bodyPoint[BodyPoint::RightHeel] = bodyPoint[BodyPoint::RightToe] -
            bodyLine[BodyLine::RightFoot];
    bodyPoint[BodyPoint::LeftKnee] = bodyPoint[BodyPoint::LeftHeel] -
            bodyLine[BodyLine::LeftLowerLeg];
    bodyPoint[BodyPoint::RightKnee] = bodyPoint[BodyPoint::RightHeel] -
            bodyLine[BodyLine::RightLowerLeg];
    bodyPoint[BodyPoint::LeftHip] = bodyPoint[BodyPoint::LeftKnee] -
            bodyLine[BodyLine::LeftUpperLeg];
    bodyPoint[BodyPoint::RightHip] = bodyPoint[BodyPoint::RightKnee] -
            bodyLine[BodyLine::RightUpperLeg];
    bodyPoint[BodyPoint::BodyBottom] = ma::midPoint(
            bodyPoint[BodyPoint::LeftHip],
            bodyPoint[BodyPoint::RightHip]);
    bodyPoint[BodyPoint::BodyTop] = bodyPoint[BodyPoint::BodyBottom] -
            bodyLine[BodyLine::Body];
    bodyPoint[BodyPoint::LeftShoulder] = bodyPoint[BodyPoint::BodyTop] -
            bodyLine[BodyLine::Shoulders]/2;
    bodyPoint[BodyPoint::RightShoulder] = bodyPoint[BodyPoint::BodyTop] +
            bodyLine[BodyLine::Shoulders]/2;
    bodyPoint[BodyPoint::LeftElbow] = bodyPoint[BodyPoint::LeftShoulder] +
            bodyLine[BodyLine::LeftUpperArm];
    bodyPoint[BodyPoint::RightElbow] = bodyPoint[BodyPoint::RightShoulder] +
            bodyLine[BodyLine::RightUpperArm];
    bodyPoint[BodyPoint::LeftWrist] = bodyPoint[BodyPoint::LeftElbow] +
            bodyLine[BodyLine::LeftLowerArm];
    bodyPoint[BodyPoint::RightWrist] = bodyPoint[BodyPoint::RightElbow] +
            bodyLine[BodyLine::RightLowerArm];
    bodyPoint[BodyPoint::Nose] = bodyPoint[BodyPoint::BodyTop] -
            bodyLine[BodyLine::Chin];
    bodyPoint[BodyPoint::Head] = bodyPoint[BodyPoint::Nose] -
            bodyLine[BodyLine::Forehead];
}

void IMUBodyPanel::stopErase(wxEraseEvent& event)
{
    // Intercepting erase event to stop erasing.
}

void IMUBodyPanel::paint(wxPaintEvent& event)
{
    if (imuBodyControl->isLoopPaused()) return;
    // Update panel coordinates.
    wxSize windowSize = this->GetSize();
    int width = windowSize.GetWidth();
    int height = windowSize.GetHeight();
    int xCentre = width / 2;
    int yCentre = height / 2;
    double scale = (height > width) ? width / 100.0 : height / 100.0;

    // Initialise draw vectors.
    for (unsigned int line = 0; line < bodyLines; ++line)
        bodyLine[line] = initialBodyLine[line];

    // Update draw vectors using IMU orientation and quaternion information.
    ma::IMUDataCollector* data = imuBodyControl->getDataCollector();
    IMUPlacement* placement = imuBodyControl->getIMUPlacement();
    unsigned int imus = data->getIMUs();
    if (placement->getIMUs() < imus) imus = placement->getIMUs();
    for (unsigned int imu = 0; imu < imus; imu++)
    {
        // Take the conjugate to convert between IMU and world space.
        ma::Quaternion rotation = data->getCurrentQuaternion(imu).conjugate();
        if (rotation.getW() == 0)
            std::cerr << "Quaternion: " << rotation << std::endl;

        // Correct for IMU orienation.
        ma::Quaternion orientation = placement ->getOrientation(imu);
        if (orientation.getW() == 0)
            std::cerr << "Orientation: " << orientation << std::endl;
        rotation = orientation * rotation * orientation.conjugate();

        // Update required line.
        unsigned int locationIndex = placement->getLocationIndex(imu);
        if (locationIndex == 0) continue;
        if (locationIndex == 1)
            bodyLine[0] = ma::rotate(bodyLine[0], rotation);
        bodyLine[locationIndex] = ma::rotate(bodyLine[locationIndex], rotation);
    }

    // Create drawing points.
    updateBodyPoints(bodyLine);
    wxPoint drawPoint[bodyPoints];
    for (unsigned int point = 0; point < bodyPoints; point++)
    {
        // Adjust points for viewing angle.
        bodyPoint[point] = rotate(bodyPoint[point], viewOrientation);
        drawPoint[point] = wxPoint(
            xCentre + (bodyPoint[point][0] * scale),
            yCentre - (bodyPoint[point][2] * scale));
    }

    // Create buffered paint event device context.
    wxBitmap buffer(windowSize);
    wxBufferedPaintDC dc(this, buffer);
    dc.SetBrush(*wxBLACK);
    dc.DrawRectangle(0, 0, width, height);

    // Draw body.
    dc.SetPen(wxPen(*wxWHITE, 10));
    dc.DrawLine(drawPoint[0], drawPoint[1]);    // Forehead.
    dc.DrawLine(drawPoint[1], drawPoint[2]);    // Chin
    dc.DrawLine(drawPoint[2], drawPoint[3]);    // Body.
    dc.DrawLine(drawPoint[4], drawPoint[5]);    // Shoulders.
    dc.DrawLine(drawPoint[6], drawPoint[7]);    // Hips.
    dc.DrawLine(drawPoint[6], drawPoint[8]);    // Left upper leg.
    dc.DrawLine(drawPoint[7], drawPoint[9]);    // Right upper leg.
    dc.DrawLine(drawPoint[8], drawPoint[10]);   // Left lower leg.
    dc.DrawLine(drawPoint[9], drawPoint[11]);   // Right lower leg.
    dc.DrawLine(drawPoint[10], drawPoint[12]);  // Left foot.
    dc.DrawLine(drawPoint[11], drawPoint[13]);  // Right foot.
    dc.DrawLine(drawPoint[4], drawPoint[14]);   // Left upper arm.
    dc.DrawLine(drawPoint[5], drawPoint[15]);   // Right upper arm.
    dc.DrawLine(drawPoint[14], drawPoint[16]);  // Left lower arm.
    dc.DrawLine(drawPoint[15], drawPoint[17]);  // Right lower arm.
}

void IMUBodyPanel::onIdle(wxIdleEvent& evt)
{
    Refresh();
}

const unsigned int IMUBodyPanel::bodyPoints = 18;

const unsigned int IMUBodyPanel::bodyLines = 15;

const ma::Vector3D IMUBodyPanel::initialBodyLine[] =
{
    ma::Vector3D(0, 2, -5),     // Forehead
    ma::Vector3D(0, -2, -5),    // Chin
    ma::Vector3D(0, 0, -20),    // Body
    ma::Vector3D(20, 0, 0),     // Shoulders
    ma::Vector3D(10, 0, 0),     // Hips
    ma::Vector3D(0, 0, -15),    // Left upper leg
    ma::Vector3D(0, 0, -15),    // Right upper leg
    ma::Vector3D(0, 0, -15),    // Left lower leg
    ma::Vector3D(0, 0, -15),    // Right lower leg
    ma::Vector3D(0, 5, 0),      // Left foot
    ma::Vector3D(0, 5, 0),      // Right foot
    ma::Vector3D(0, 0, -15),    // Left upper arm
    ma::Vector3D(0, 0, -15),    // Right upper arm
    ma::Vector3D(0, 0, -15),    // Left lower arm
    ma::Vector3D(0, 0, -15),    // Right lower arm
};
