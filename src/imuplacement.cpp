// (c) 2018: Marcel Admiraal

#include "imuplacement.h"

#include <fstream>

IMUPlacement::IMUPlacement() :
    imus(0)
{
    locationIndex = new unsigned int[imus]();
    orientation = new ma::Quaternion[imus];
    if (!loadIMUPlacements("imuplacements.dat")) setIMUs(1);
}

IMUPlacement::~IMUPlacement()
{
    if (imus)
    {
        delete[] locationIndex;
        delete[] orientation;
    }
}

ma::Quaternion IMUPlacement::getOrientation(const unsigned int imu) const
{
    if (imu < imus)
        return orientation[imu];
    else
        return ma::Quaternion();
}

void IMUPlacement::setOrientation(ma::Quaternion orientation,
        const unsigned int imu)
{
    if (imu < imus)
        this->orientation[imu] = orientation;
}

unsigned int IMUPlacement::getLocationIndex(const unsigned int imu) const
{
    if (imu < imus)
        return locationIndex[imu];
    else
        return 0;
}

void IMUPlacement::setLocationIndex(unsigned int locationIndex,
        const unsigned int imu)
{
    if (imu < imus)
        this->locationIndex[imu] = locationIndex;
}

unsigned int IMUPlacement::getIMUs() const
{
    return imus;
}

void IMUPlacement::setIMUs(unsigned int newIMUs)
{
    if (newIMUs != imus)
    {
        unsigned int* newLocationIndex = new unsigned int[newIMUs]();
        ma::Quaternion* newOrientation = new ma::Quaternion[newIMUs];
        // Keep existing information if possible.
        unsigned int imu = 0;
        for (; imu < imus && imu < newIMUs; ++imu)
        {
            newLocationIndex[imu] = locationIndex[imu];
            newOrientation[imu] = orientation[imu];
        }
        // Set default values.
        for (; imu < newIMUs; ++imu)
        {
            newLocationIndex[imu] = 0;
            newOrientation[imu] = ma::Quaternion(1);
        }
        std::swap(locationIndex, newLocationIndex);
        std::swap(orientation, newOrientation);
        delete[] newLocationIndex;
        delete[] newOrientation;
        imus = newIMUs;
    }
}

bool IMUPlacement::saveIMUPlacements(const char* filename)
{
    std::ofstream fileStream(filename, std::ofstream::out);
    if (!fileStream.is_open()) return false;
    fileStream << *this;
    if (!fileStream.good()) return false;
    return true;
}

bool IMUPlacement::loadIMUPlacements(const char* filename)
{
    std::ifstream fileStream(filename, std::ifstream::in);
    if (!fileStream.is_open()) return false;
    fileStream >> *this;
    if (!fileStream.good()) return false;
    return true;
}

std::ostream& operator<<(std::ostream& os, const IMUPlacement& source)
{
    unsigned int imus = source.getIMUs();
    os << imus << std::endl;
    for (unsigned int imu = 0; imu < imus; ++imu)
    {
        os << source.getLocationIndex(imu) << "\t";
        os << source.getOrientation(imu) << std::endl;
    }
    return os;
}

std::istream& operator>>(std::istream& is, IMUPlacement& destination)
{
    unsigned int imus;
    unsigned int* locationIndex;
    ma::Quaternion* orientation;

    is >> imus;
    if (!is.good()) return is;
    locationIndex = new unsigned int[imus];
    orientation = new ma::Quaternion[imus];
    for (unsigned int imu = 0; imu < imus; ++imu)
    {
        is >> locationIndex[imu];
        is >> orientation[imu];
    }

    if (!is.good()) return is;
    destination.setIMUs(imus);
    for (unsigned int imu = 0; imu < imus; ++imu)
    {
        destination.setLocationIndex(locationIndex[imu], imu);
        destination.setOrientation(orientation[imu], imu);
    }
    return is;
}

