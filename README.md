# IMUBody #

Cross-platform C++ code for a graphical user interface to visualise the tracking of body motion using imus.

## Dependencies ##
This code depends on:

* [wxWidgets](https://www.wxwidgets.org/)
* [MAUtils](https://bitbucket.org/madmiraal/mautils)
* [MAwxUtils](https://bitbucket.org/madmiraal/mawxutils)
* [MAIMU](https://bitbucket.org/madmiraal/maimu)
* [MADSP](https://bitbucket.org/madmiraal/madsp)
* [MAIMUData](https://bitbucket.org/madmiraal/maimudata)